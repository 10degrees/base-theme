<?php

namespace App\Listeners;

use App\Services\BaseWrapper;
use TenDegrees\Foundation\Application;

class BaseWrapperTemplate
{
    /**
     * The app instance
     *
     * @var Application
     */
    protected Application $app;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle the event.
     *
     * @param  string  $template
     * @return void
     */
    public function handle(string $template)
    {
        return $this->app->instance(BaseWrapper::class, new BaseWrapper($template));
    }
}

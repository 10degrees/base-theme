<?php

namespace App\Providers;

use App\Services\BaseWrapper;
use TenDegrees\Foundation\Providers\WordPressServiceProvider as ServiceProvider;

class WordPressServiceProvider extends ServiceProvider
{
    /**
     * The post types to register
     *
     * @var array
     */
    protected $postTypes = [
        //
    ];

    /**
     * The shortcodes to register
     *
     * @var array
     */
    protected $shortcodes = [
        //
    ];

    /**
     * The admin pages to register
     *
     * @var array
     */
    protected $adminPages = [
        //
    ];

    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BaseWrapper::class, function ($app, $args) {
            return new BaseWrapper(...$args);
        });
    }
}

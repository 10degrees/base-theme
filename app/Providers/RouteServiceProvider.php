<?php

namespace App\Providers;

use TenDegrees\Foundation\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The REST namespace
     *
     * @var string
     */
    protected static $restNamespace = 'api';

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        require $this->app->basePath('routes/ajax.php');

        require $this->app->basePath('routes/api.php');

        //
    }
}

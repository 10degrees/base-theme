<?php

namespace App\Services;

class BaseWrapper
{
    /**
     * The main template path
     *
     * @var string
     */
    protected string $mainTemplate;

    /**
     * The base path
     *
     * @var string
     */
    protected string $base;

    /**
     * An array of the templates
     *
     * @var array
     */
    protected array $templates = ['base.php'];

    /**
     * Create the instance
     *
     * @param string $template
     */
    public function __construct(string $template)
    {
        $this->mainTemplate = $template;

        if ($template !== 'index.php') {
            array_unshift($this->templates, sprintf('base-%s', $template));
        }
    }

    /**
     * Require the template
     *
     * @return void
     */
    public function require(): void
    {
        require $this->mainTemplate;
    }

    /**
     * Convert the instane to a template path
     *
     * @return string
     */
    public function __toString(): string
    {
        return locate_template($this->templates);
    }
}

<!doctype html>

<html class="no-js" <?php language_attributes(); ?>>

<?php echo td_view('head'); ?>

<body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <a href="#main" class="sr-only focus:not-sr-only">
        <?php esc_html_e('Skip to main content', '@textdomain'); ?>
    </a>
    <a href="#footer" class="sr-only focus:not-sr-only">
        <?php esc_html_e('Skip to footer', '@textdomain'); ?>
    </a>

    <?php
    do_action('get_header');
    echo td_view('header');
    ?>

    <main id="main">
        <?php echo td_view('breadcrumbs'); ?>
        <?php td_app(\App\Services\BaseWrapper::class)->require(); ?>
    </main>

    <?php echo td_view('footer');
    ?>

</body>

</html>
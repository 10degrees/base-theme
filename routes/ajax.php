<?php

use TenDegrees\Support\Facades\Route;

Route::ajax('welcome', function () {
    return 'Hello World!';
});

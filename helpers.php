<?php

use TenDegrees\Foundation\Mix;
use TenDegrees\Routing\ResponseFactory;
use TenDegrees\Support\Facades\App;
use TenDegrees\Support\Facades\URL;
use TenDegrees\Support\Facades\View;

if (!function_exists('td_app')) {
    /**
     * Get the available container instance.
     *
     * @param  string|null  $abstract
     * @param  array  $parameters
     * @return mixed|\Illuminate\Contracts\Foundation\Application
     */
    function td_app(?string $abstract = null, array $parameters = [])
    {
        $instance = App::getInstance();

        return $abstract ? $instance->make($abstract, $parameters) : $instance;
    }
}

if (!function_exists('td_pagination_links')) {
    function td_pagination_links()
    {
        global $wp_query;
        $bignum = 999999999;
        if ($wp_query->max_num_pages <= 1) {
            return;
        }

        echo '<nav class="pagination">';

        echo paginate_links(
            array(
                'base' => str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum))),
                'format' => '',
                'current' => max(1, get_query_var('paged')),
                'total' => $wp_query->max_num_pages,
                'prev_text' => '&larr;',
                'next_text' => '&rarr;',
                'type' => 'list',
                'end_size' => 3,
                'mid_size' => 3,
            )
        );

        echo '</nav>';
    }
}

if (!function_exists('td_redirect')) {
    /**
     * Redirect a request
     *
     * @param string $to
     * @param int $status
     * @return void
     */
    function td_redirect(string $to, int $status = 302)
    {
        return td_url()->redirect($to, $status);
    }
}

if (!function_exists('td_url')) {
    /**
     * Generate a url for the application.
     *
     * @param string|null $path
     * @return \TenDegrees\Routing\UrlGenerator|string
     */
    function td_url(string $path = null)
    {
        if (is_null($path)) {
            return URL::getFacadeRoot();
        }

        return URL::to($path);
    }
}

if (!function_exists('td_view')) {
    /**
     * Return a view
     *
     * @param string $view
     * @param array $args
     * @return string
     */
    function td_view(string $view, array $args = []): string
    {
        return View::make($view, $args)->render();
    }
}

if (!function_exists('td_response')) {
    /**
     * Return a new response from the application.
     *
     * @param  \TenDegrees\View\View|string|array|null  $content
     * @param  int  $status
     * @param  array  $headers
     * @return \TenDegrees\Http\Response|\TenDegrees\Routing\ResponseFactory
     */
    function td_response($content = null, int $status = 200, array $headers = [])
    {
        $factory = App::make(ResponseFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($content, $status, $headers);
    }
}

if (!function_exists('td_mix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     * @return string
     *
     * @throws \Exception
     */
    function td_mix(string $path)
    {
        return App::make(Mix::class)($path, get_stylesheet_directory_uri());
    }
}

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <?php td_view('content'); ?>
    <?php endwhile; ?>
<?php else : ?>
    <div class="text-red-500">
        <p><?php _e('Sorry, no results were found.', '@textdomain'); ?></p>
    </div>
    <?php get_search_form(); ?>
<?php endif; ?>

<?php td_pagination_links(); ?>
/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./base.php", "./resources/**/*.{php,js}"],
    theme: {
        extend: {},
    },
    plugins: [],
};

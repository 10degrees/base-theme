# 10 Degrees Base Theme

The 10 Degrees base theme is a starter theme using the `td/framework` package to streamline WordPress theme development.

## Installation

From the `wp-content/themes` directory of your project:

```
composer create-project td/theme [theme-name]
```

Find and replace `@theme` and `@textdomain` to your theme name and textdomain respectively.

### NPM

The theme comes with a basic laravel-mix setup.

```
npm install
```
There are some commands to run a dev build, production build, or to watch files:
```
npm run dev
npm run prod
npm run watch
```

## Using the framework

The `td/framework` package contains some useful `wp-cli` commands. View the [framework readme](https://bitbucket.org/10degrees/base-framework/src/main/README.md) for more information on using the framework.

## Deployment

To deploy the theme, you will need to run the following actions from within the theme directory.

```
composer install
npm install
npm run prod
```